import {Component, OnInit} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { WeatherViewModel } from './model/WeatherViewModel';
import { WeatherResult } from './model/WeatherResult';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./css/sb-admin.css']
})

export class WeatherComponent implements OnInit {
  model: WeatherViewModel = {
    city: 'Santiago, CL',
    units: 'us'
  };
  cities: string[] = ['Santiago, CL'];
  weatherResult: WeatherResult;

  constructor(private http: HttpClient) {
  }
  ngOnInit() {
    this.getCities();
  }
  getWeather(): void {
    const url = 'http://localhost:8080/weatherR?city=' + this.model.city + '&units=' + this.model.units;
    this.http.get(url).subscribe(
    data  => {
      this.weatherResult = <any>data;
      this.weatherResult.city = this.model.city;
    console.log('GET Request is successful.');
    },
    error  => {
    console.log('Error', error);
    }
    );
  }

  getCities(): void {
  const url = 'http://localhost:8080/cities';
  this.http.get(url).subscribe(
  data  => {
  this.cities = <any>data;
  console.log('GET Request is successful', data);
  },
  error  => {
  console.log('Error', error);
  }
  );
}

}

