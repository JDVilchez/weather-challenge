## Weather challenge

to run spring api project, go to WeatherChallenge and execute "mvn spring-boot:run"

to run Angular client project, go to weather-ng-app and execute "npm install" to install all dependencies, then execute "ng serve" to run it

To open Angular view, after running both projects, go to http://localhost:4200/

Beside the angular client project, there is a view inside the Spring Project. Test it using http://localhost:8080/weather

To open Spring project in eclipse, open weather-challenge using eclipse, then File->import->Existing Maven Projects->Select WeatherChallenge.

enjoyed.