package com.latam.test.manager.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.latam.test.client.RestClient;
import com.latam.test.manager.WeatherManager;
import com.latam.test.model.City;
import com.latam.test.model.WeatherResult;
import com.latam.test.utils.WeatherUtils;

@RunWith(MockitoJUnitRunner.class)
public class WeatherManagerTest {
	
	@Mock
	private RestClient client;
	
	@Mock
	private WeatherUtils utils;
	
	private WeatherManager manager;

	@Before
	public void setUp() {
		manager = new WeatherManager(client, utils);
	}

	@Test
	public void getWeatherByCityAndMetric_whenCallClient_getWeather() {
		String city = "dummy city";
		String metric = "dummy metric";
		
		String responseClient = "dummy client response";
		WeatherResult responseUtil = new WeatherResult();
		responseUtil.setBase("test ok");
		
		when(utils.mappingObject(anyString(), eq(WeatherResult.class))).
		thenReturn(responseUtil);
		
		when(client.get(anyString(), anyString())).
		thenReturn(responseClient);
		
		WeatherResult result = manager.getWeatherByCityAndMetric(city, metric);
		
		assertEquals(responseUtil, result);
	}
	
	@Test
	public void getCities_whenGetFile_shouldReturnCities() {
		
		City city = new City();
		city.setName("Santiago");
		city.setCountry("CL");
		
		City[] responseUtilMapping = {city};
		String responseUtilFile = "dummy file";
		
		when(utils.mappingObject(anyString(), eq(City[].class))).
		thenReturn(responseUtilMapping);
		
		when(utils.getFileContent(anyString())).
		thenReturn(responseUtilFile);
		
		
		String[] expectedResult = {"Santiago, CL"};
		String[] result = manager.getCities();
		
		assertArrayEquals(expectedResult, result);
	}
}