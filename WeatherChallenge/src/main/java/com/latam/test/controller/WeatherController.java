package com.latam.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.latam.test.manager.WeatherManager;
import com.latam.test.model.WeatherResult;

@Controller
@ComponentScan(basePackages={"com.latam.test.manager"})
class WeatherController {
	
	WeatherResult wr = null;
	String city = "";
	String[] cities = null;
	
	@Autowired
	WeatherManager weatherManager;

    @PostMapping("/weather")
    public String handlePostRequest(String city, String metric) {
    	wr = weatherManager.getWeatherByCityAndMetric(city, metric);
    	this.city = city;
    	
        return "redirect:/weather";
    }
    
    @GetMapping("/weather")
    public String handleGetRequest(Model model) {
    	
    	cities = weatherManager.getCities();
    	
    	model.addAttribute("wr", wr);
    	model.addAttribute("city", city);
    	model.addAttribute("cities", cities);
        return "weather";
    }

}