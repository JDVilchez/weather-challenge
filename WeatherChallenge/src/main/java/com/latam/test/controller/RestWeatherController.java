package com.latam.test.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.latam.test.client.RestClient;
import com.latam.test.manager.WeatherManager;
import com.latam.test.model.WeatherResult;
import com.latam.test.utils.WeatherUtils;

@RestController
@ComponentScan(basePackages={"com.latam.test.client", "com.latam.test.manager", "com.latam.test.utils"})
class RestWeatherController {
	
	@Autowired
	RestClient client;
	
	@Autowired
	WeatherManager weatherManager;
	
	@Autowired
	WeatherUtils utils;

    
    @GetMapping("/weatherR")
    @CrossOrigin(origins = "http://localhost:4200")
    public WeatherResult handleGetRequest(String city, String units) {
    	System.out.println(city + " " + units);
    	return weatherManager.getWeatherByCityAndMetric(city, units);
    }
    
    @PostMapping("/weatherR")
    @CrossOrigin(origins = "http://localhost:4200")
    public WeatherResult handlePostRequest(@RequestParam String city, @RequestParam String units) {
    	System.out.println(city + " " + units);
    	return weatherManager.getWeatherByCityAndMetric(city, units);
    }
    
    @GetMapping("/cities")
    @CrossOrigin(origins = "http://localhost:4200")
    public String[] cities() {
    	return weatherManager.getCities();
    }
    
    
}