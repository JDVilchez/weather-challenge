package com.latam.test.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import com.latam.test.client.RestClient;
import com.latam.test.model.City;
import com.latam.test.model.WeatherResult;
import com.latam.test.utils.WeatherUtils;

@Service
@ComponentScan(basePackages={"com.latam.test.client","com.latam.test.utils"})
public class WeatherManager {
	
	@Autowired
	RestClient client;
	
	@Autowired
	WeatherUtils utils;
	
	public WeatherManager(){
		
	}
	
	public WeatherManager(RestClient client, WeatherUtils utils){
		this.client = client;
		this.utils = utils;
	}

    public WeatherResult getWeatherByCityAndMetric(String city, String metric) {
    	WeatherResult wr = (WeatherResult) utils.mappingObject(client.get(city, metric), WeatherResult.class);
    	return wr;
    }
    
    public String[] getCities() {
    	String json = utils.getFileContent("citylist.json");
    	
    	City[] citiesW = (City[]) utils.mappingObject(json, City[].class);
    	
    	String[] cities = new String[citiesW.length];
    	for (int i = 0; i < citiesW.length; i++) {
    		cities[i] = citiesW[i].getName() + ", " + citiesW[i].getCountry();
		}
    	
    	return cities;
    }

}