package com.latam.test.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.latam.test.utils.AppProperties;

@Service
@ComponentScan(basePackages = { "com.latam.test.utils" })
public class RestClient {

	private RestTemplate rest;
	private HttpHeaders headers;
	private HttpStatus status;

	@Autowired
	AppProperties props;

	public RestClient() {
		this.rest = new RestTemplate();
		this.headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "*/*");
	}

	public String get(String city, String units) {
		HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
		ResponseEntity<String> responseEntity = rest.exchange(
				props.getWeatherserver() 
				+ "/data/2.5/weather?q=" 
				+ city 
				+ "&units=" 
				+ units 
				+ "&APPID=" 
				+ props.getAppid(), 
				HttpMethod.GET,
				requestEntity, String.class);
		this.setStatus(responseEntity.getStatusCode());
		return responseEntity.getBody();
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}