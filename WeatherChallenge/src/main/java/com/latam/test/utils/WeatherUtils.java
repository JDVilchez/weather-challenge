package com.latam.test.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WeatherUtils {

	public Object mappingObject(String json, Class cl) {

		Object result = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			result = objectMapper.readValue(json, cl);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;

	}
	
	public String getFileContent(String path){
    	String content = "";
    	try {
    		File file = ResourceUtils.getFile("classpath:"+path);
			byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
			content =  new String(encoded, "UTF-8");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	return content;
    }

}
