package com.latam.test.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class AppProperties {

	private String appid;
	private String weatherserver;

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getWeatherserver() {
		return weatherserver;
	}

	public void setWeatherserver(String weatherserver) {
		this.weatherserver = weatherserver;
	}

}